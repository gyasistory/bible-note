package com.storytail.bible_note;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/**
 * @author Gyasi
 *
 */
public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private String mDrawerTitle;
    private CharSequence mTitle;
    public String[] mSomeThing;
    public Context mContext;

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;


        mTitle = mDrawerTitle = String.valueOf(R.string.str_title);
        mSomeThing = getResources().getStringArray(R.array.SomeThing);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer);
        //setFragment();

        //---set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        //---set up drawer's list View with item and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mSomeThing));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        //---enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        //---ActionBarDrawerToggle ties together the proper interacitons
        //---between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                   /* host Activity*/
                mDrawerLayout,          /* DrawLayout object*/
                R.drawable.ic_drawer,   /* nav drawer image to replace  'Up' caret */
                R.string.drawer_open,   /* "open drawer" description for accessibility */
                R.string.drawer_close   /*" close drawer" description for accessiblity */
        ){
            @Override
            public void onDrawerClosed(View drawerView) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();    // creates call to onPrepareOptionMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();    // creates call to onPrepareOptionMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if(savedInstanceState == null){
            selectedItem(0);
        }




    }

    /*private void setFragment() {

        Fragment mainFragment = new HomePage();

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.mainContainer, mainFragment).commit();

    }
    */

    private void selectedItem(int position) {
        /*Fragment mainFragment;
        FragmentManager fm;
        switch (position){
            case 0:
                mainFragment = new HomePage();

                fm = getFragmentManager();
                fm.beginTransaction().replace(android.R.id.content, mainFragment).commit();
                break;
            case 1:
                mainFragment = new Note();

                fm = getFragmentManager();
                fm.beginTransaction().replace(android.R.id.content, mainFragment).commit();

        } */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*  Called whenever we call  invalidateOptionMenu() */

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar hom/up action should  open or close the drawer.
        // ActionBarToggle will take care of this .
        if (mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()){
            case R.id.action_websearch:
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            Toast.makeText(mContext, view.toString() + "\r\n" + i, Toast.LENGTH_LONG).show();
            selectedItem(i);

        }

    }


}
