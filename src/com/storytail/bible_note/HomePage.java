package com.storytail.bible_note;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by gstory on 5/30/13.
 *
 * This is the activity for the start Screen
 */
public class HomePage extends FragmentActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}